using GameCreator.Runtime.Cameras;
using GameCreator.Runtime.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameState state;

    public static event Action<GameState> OnStateChange;
    public List<GameObject> Entities;
    private Camera cam;
    //private PlayerInputNova playerInputNova;

    [Header("UI Elements")]
    public GameObject MainMenuUI;
    public GameObject MainGameUI;
    public GameObject WinUI;
    public GameObject LoseUi;


    private void Awake()
    {
        Instance = this;
        cam = Camera.main;
    }

    private void Start()
    {
        updateGameState(GameState.MainMenu);
    }

    public void updateGameState(GameState newState)
    {
        state = newState;

        switch (newState)
        {
            case GameState.MainMenu:
                HandleMainMenu();
                break;
            case GameState.MainGame:
                HandleMainGame();
                break;
            case GameState.Win:
                HandleWin();
                break;
            case GameState.Lose:
                HandleLose();
                break;
            case GameState.Restart:
                HandleRestart();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }

        OnStateChange?.Invoke(state);

    }

    public void HandleRestart()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
    }

    public void HandleLose()
    {
        // SHOW LOSE MENU
        disableEntities();
    }

    public void HandleWin()
    {
        // SHOW WIN MENU
        disableEntities();
    }

    public void HandleMainGame()
    {
        enableEntities();
    }

    public void HandleMainMenu()
    {
        // SHOW MAIN MENU
        disableEntities();
    }

    private void disableEntities()
    {
        foreach (GameObject e in Entities)
        {
            e.SetActive(false);
        }
    }

    private void enableEntities()
    {
        foreach (GameObject e in Entities)
        {
            e.SetActive(true);
        }
    }

    private void Update()
    {
        // Debug Restart
        if (Input.GetKey(KeyCode.R))
        {
            updateGameState(GameState.Restart);
        }
    }
}

public enum GameState
{
    MainMenu,
    MainGame,
    Win,
    Lose,
    Restart
}
