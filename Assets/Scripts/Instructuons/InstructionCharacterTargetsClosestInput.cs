﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameCreator.Runtime.Characters;
using GameCreator.Runtime.Common;
using UnityEngine;
using Character = GameCreator.Runtime.Characters.Character;

namespace GameCreator.Runtime.VisualScripting {
    [Title("Cycle Closest Target Input Biased")]
    [Description("Cycles to the closest candidate target to the character from the Targets list based on Input")]

    [Category("Characters/Combat/Targeting/Cycle Closest Target Input Biased")]

    [Parameter("Character", "The Character that attempts to change its candidate target")]
    [Parameter("Layer Mask", "What Layer the enemies are on.")]

    [Keywords("Character", "Combat", "Focus", "Pick", "Candidate", "Targets")]
    [Image(typeof(IconBullsEye), ColorTheme.Type.Yellow, typeof(OverlayDot))]

    [Serializable]
    public class InstructionCharacterTargetsClosestInput : Instruction
    {
        [SerializeField] private PropertyGetGameObject m_Character = GetGameObjectPlayer.Create();
        [SerializeField] private LayerMask layerMask;

        public override string Title => $"Cycle Closest Target from {this.m_Character} with Input Bias";

        protected override Task Run(Args args)
        {
            Character character = this.m_Character.Get<Character>(args);
            if (character == null) return DefaultResult;

            Targets targets = character.Combat.Targets;
            List<GameObject> list = targets.List;
            Vector3 inputDirection = character.Player.InputDirection;
            GameObject nextTarget = null;

            //var camera = Camera.main;
            //var forward = camera.transform.forward;
            //var right = camera.transform.right;

            //forward.y = 0f;
            //right.y = 0f;

            //forward.Normalize();
            //right.Normalize();

            //Debug.Log(inputDirection.ToString());
            //inputDirection = forward * inputDirection.z + right * inputDirection.x;
            inputDirection = inputDirection.normalized;
            

            //if(inputDirection == Vector3.zero) {
            //    CycleTargets.Closest(character);
            //    return DefaultResult;
            //}

            RaycastHit info;

            if (Physics.SphereCast(character.transform.position, 3f, inputDirection, out info, 10f, layerMask))
            {
                if(info.collider.gameObject.GetComponent<Character>() != null)
                {
                    Debug.Log("Target Found.");
                    nextTarget = info.collider.gameObject;
                    if(targets.List.Contains(nextTarget))
                    {
                        targets.Primary = nextTarget;
                    }
                }
            }
            else if(Physics.OverlapSphere(character.transform.position, 2f, layerMask).Length > 0)
            {
                Debug.Log("Target Close by");
                CycleTargets.Closest(character);
            }

            return DefaultResult;
        }
    }
}
