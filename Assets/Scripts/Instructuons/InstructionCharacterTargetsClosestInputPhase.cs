﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameCreator.Runtime.Characters;
using GameCreator.Runtime.Common;
using GameCreator.Runtime.Melee;
using GameCreator.Runtime.VisualScripting;
using UnityEngine;

namespace GameCreator.Runtime.VisualScripting
{
    [Title("Cycle Closest Target in Phase Input Biased")]
    [Description("Cycles to the closest candidate target to the character from the Targets list based on Input, Filtering for Melee Phase")]

    [Category("Characters/Combat/Targeting/Cycle Closest Target in Phase Input Biased")]

    [Parameter("Character", "The Character that attempts to change its candidate target")]
    [Parameter("Layer Mask", "What Layer the enemies are on.")]
    [Parameter("Phases", "The attack phases the character might be in")]

    [Keywords("Character", "Combat", "Focus", "Pick", "Candidate", "Targets")]
    [Image(typeof(IconBullsEye), ColorTheme.Type.Yellow, typeof(OverlayDot))]

    [Serializable]
    public class InstructionCharacterTargetsClosestInputPhase : Instruction
    {
        [SerializeField] private PropertyGetGameObject m_Character = GetGameObjectPlayer.Create();
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private MeleePhaseMask m_Phases = MeleePhaseMask.None;

        public override string Title => $"Cycle Closest Target from {this.m_Character}, who is currently {this.m_Phases} with Input Bias";

        protected override Task Run(Args args)
        {
            Character character = this.m_Character.Get<Character>(args);
            if (character == null) return DefaultResult;

            Targets targets = character.Combat.Targets;
            GameObject currentTarget = targets.Primary;
            List<GameObject> list = targets.List;
            Vector3 inputDirection = character.Player.InputDirection;
            //GameObject nextTarget = null;

            inputDirection = inputDirection.normalized;
            //Debug.Log(inputDirection.ToString());

            MeleeStance melee = currentTarget.GetComponent<Character>().Combat.RequestStance<MeleeStance>();
            if (((int)melee.CurrentPhase & (int)this.m_Phases) > 0)
            {
                targets.Primary = currentTarget;
                return DefaultResult;
            }

            Collider[] enemies = Physics.OverlapSphere(character.transform.position, 3f, layerMask);

            if(enemies != null && enemies.Length != 0)
            {
                foreach(Collider c in enemies)
                {
                    GameObject candidate = c.gameObject;
                    melee = candidate.GetComponent<Character>().Combat.RequestStance<MeleeStance>();
                    if (((int)melee.CurrentPhase & (int)this.m_Phases) > 0)
                    {
                        targets.Primary = candidate;
                        return DefaultResult;
                    }
                }
            }

            return DefaultResult;
        }
    }
}