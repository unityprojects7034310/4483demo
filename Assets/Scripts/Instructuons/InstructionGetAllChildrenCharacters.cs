﻿using System;
using System.Threading.Tasks;
using GameCreator.Runtime.Characters;
using GameCreator.Runtime.Common;
using GameCreator.Runtime.Variables;
using Unity.VisualScripting;
using UnityEngine;

namespace GameCreator.Runtime.VisualScripting
{
    [Title("Get Children Characters")]
    [Description("Gets all children of the attached gameobject which contain a Character component, and stores them in a List Variable.")]

    [Category("Characters/Get Children Characters")]

    [Parameter("GameObject", "The GameObject to search for children who are Characters")]
    [Parameter("List Variable", "The List Variable to store the children in.")]

    [Keywords("Character", "Children", "GameObject")]

    [Serializable]
    public class InstructionGetAllChildrenCharacters : Instruction
    {
        [SerializeField] private GameObject parent;
        [SerializeField] private LocalListVariables localList;

        protected override Task Run(Args args)
        {
            localList.Clear();
            Character[] characters = parent.GetComponentsInChildren<Character>();
            foreach(Character c in characters)
            {
                localList.Push(c.gameObject);
            }
            return DefaultResult;
        }
    }
}