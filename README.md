# Grieving Vengeance - Combat Demo

---

**Introduction**

Welcome to the combat demo of "Grieving Vengeance," a game where skillful combat and strategic movements blend to offer an immersive experience. This manual provides a comprehensive guide to mastering both basic and combat controls, whether you prefer a keyboard and mouse setup or a game controller. Immerse yourself in a free-flow combat system designed for dynamic engagements and seamless target transitions, echoing the fluidity found in iconic games like the Batman Arkham series.

---

**BASIC CONTROLS**

**Keyboard + Mouse:**

- **WASD:** Move your character through the game world.
- **Mouse:** Look around and adjust your view.
- **Spacebar:** Sprint to traverse the world faster.

**Controller:**

- **Left Stick:** Navigate your player.
- **Right Stick:** Control the camera angle.
- **Button South (A/✕):** Sprint to traverse the world faster.

*Note: Using a controller is recommended for the best gaming experience, though it is not a requirement.*

---

**COMBAT CONTROLS**

**Keyboard + Mouse:**

- **Left Click:** Execute attacks.
- **Right Click:** Perform counters.
- **E:** Initiate takedown moves.

**Controller:**

- **Button West (X/A):** Attack.
- **Button North (Y/△):** Counter.
- **Button East (B/○):** Execute takedowns.

---

**COMBAT SYSTEM DESCRIPTION**

"Grieving Vengeance" features a state-of-the-art freeflow combat system that allows players to dynamically target and switch between enemies based on their input. This system enables seamless movement across the battlefield, enhancing the combat experience. Here are the key elements of the combat system:

- **Targeting and Movement:** Pressing the movement direction targets enemies in that vicinity. A motion warp effect allows for fluid transitions between enemies during combat.
- **Countering:** The counter mechanism lets you block and retaliate against incoming enemy strikes. Timing is crucial to executing successful counters.
- **Takedowns:** Accumulating a combo of 5 hits enables a powerful takedown move, instantly incapacitating an opponent. Takedowns require strategic build-up and can only be performed after achieving the required combo threshold. Note that receiving damage from enemies resets your combo count.

**Objective:** Your mission is to defeat all enemies within the area to emerge victorious.

**Restarting the Game:** Should you encounter any game-breaking issues, press **R** to restart the demo and reset your progress.


---

HAVE FUN :)
